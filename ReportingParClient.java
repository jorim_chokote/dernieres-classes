import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;

public class ReportingParClient extends JFrame {

	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * containeur de la fenetre
	 */
	private JPanel contentPane;
	
	/**
	 * label pour le reporting quotidien
	 */
	private JLabel lblReportingQuotidien;
	
	/**
	 * label pour le reporting mensuel
	 */
	private JLabel lblReportingMensuel;
	
	/**
	 * label pour le mois
	 */
	private JLabel lblMois;
	
	/**
	 * label annee dans le cadre du reporting mensuel
	 */
	private JLabel lblAnnee1;
	
	/**
	 * label date
	 */
	private JLabel lblDate;
	
	/**
	 * label pour le reporting annuel
	 */
	private JLabel lblReportingAnnuel;
	
	/**
	 * label pour l'annee dans le cadre du reporting annuel
	 */
	private JLabel lblAnnee2;
	
	/**
	 * zone de texte pour la date
	 */
	private JTextField textFieldDate;
	
	/**
	 * zone de texte pour le mois
	 */
	private JTextField textFieldMois;
	
	/**
	 * zone de texte pour l'annee dans le cadre du reporting mensuel
	 */
	private JTextField textFieldAnnee1;
	
	/**
	 * zone de texte pour l'annee dans le cadre du reporting annuel
	 */
	private JTextField textFieldAnnee2;
	
	/**
	 * bouton pour afficher le reporting quotidien
	 */
	private JButton btnValiderQuotidien;
	
	/**
	 * bouton pour afficher le reporting mensuel
	 */
	private JButton btnValiderMensuel;
	
	/**
	 * bouton pour afficher le reporting annuel
	 */
	private JButton btnValiderAnnuel;
	
	/**
	 * bouton pour quitter
	 */
	private JButton btnQuitter;
	
	/**
	 * instance de la classe ReportingDAO
	 */
	private ReportingDAO reportingDAO = new ReportingDAO();
	
	/**
	 * zone de teste pour afficher les reportings
	 */
	private JTextArea textAreaReporting = new JTextArea();
	
	/**
	 * zone de defilement pour les reportings
	 */
	private JScrollPane scrollPane = new JScrollPane();


	/**
	 * Ouverture de la fenetre
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReportingParClient frame = new ReportingParClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creation de la fenetre
	 */
	public ReportingParClient() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 480, 322);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Reporting par client");
		
		lblReportingQuotidien = new JLabel("Reporting quotidien (AAAA-MM-JJ)");
		lblReportingQuotidien.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblReportingQuotidien.setBounds(33, 24, 214, 20);
		contentPane.add(lblReportingQuotidien);
		
		textFieldDate = new JTextField();
		textFieldDate.setBounds(77, 50, 86, 30);
		contentPane.add(textFieldDate);
		textFieldDate.setColumns(10);
		
		btnValiderQuotidien = new JButton("Valider");
		btnValiderQuotidien.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnValiderQuotidien.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
					
						List<Client> liste = reportingDAO.reportingClientQuotidien(Date.valueOf(textFieldDate.getText()));
						textAreaReporting.setText("");
						// on affiche dans la console du client les client recus
						for (Client a : liste) {
							textAreaReporting.append(a.toString());
							textAreaReporting.append("\n");
						}
						if (liste.isEmpty()){
							JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Client");
						}
					} catch (Exception erreur) {
							JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
									"Erreur", JOptionPane.ERROR_MESSAGE);
					  }
			}
		});
		btnValiderQuotidien.setBounds(320, 54, 89, 23);
		contentPane.add(btnValiderQuotidien);
		
		lblReportingMensuel = new JLabel("Reporting Mensuel");
		lblReportingMensuel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblReportingMensuel.setBounds(33, 86, 119, 20);
		contentPane.add(lblReportingMensuel);
		
		lblMois = new JLabel("Mois");
		lblMois.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblMois.setBounds(33, 117, 46, 14);
		contentPane.add(lblMois);
		
		textFieldMois = new JTextField();
		textFieldMois.setBounds(77, 111, 86, 26);
		contentPane.add(textFieldMois);
		textFieldMois.setColumns(10);
		
		lblAnnee1 = new JLabel("Annee");
		lblAnnee1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblAnnee1.setBounds(173, 117, 46, 14);
		contentPane.add(lblAnnee1);
		
		textFieldAnnee1 = new JTextField();
		textFieldAnnee1.setBounds(212, 113, 86, 26);
		contentPane.add(textFieldAnnee1);
		textFieldAnnee1.setColumns(10);
		
		btnValiderMensuel = new JButton("Valider");
		btnValiderMensuel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnValiderMensuel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					List<Client> liste = reportingDAO.reportingClientMensuel(textFieldMois.getText(), textFieldAnnee2.getText());
					textAreaReporting.setText("");
					// on affiche dans la console du client les client recus
					for (Client a : liste) {
						textAreaReporting.append(a.toString());
						textAreaReporting.append("\n");
					}
					if (liste.isEmpty()){
						JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Client");
					}
				} catch (Exception erreur) {
						JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
								"Erreur", JOptionPane.ERROR_MESSAGE);
				  }
			}
		});
		btnValiderMensuel.setBounds(320, 113, 89, 23);
		contentPane.add(btnValiderMensuel);
		
		lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDate.setBounds(33, 55, 46, 14);
		contentPane.add(lblDate);
		
		lblReportingAnnuel = new JLabel("Reporting Annuel");
		lblReportingAnnuel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblReportingAnnuel.setBounds(33, 142, 107, 20);
		contentPane.add(lblReportingAnnuel);
		
		lblAnnee2 = new JLabel("Annee");
		lblAnnee2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblAnnee2.setBounds(33, 173, 46, 14);
		contentPane.add(lblAnnee2);
		
		textFieldAnnee2 = new JTextField();
		textFieldAnnee2.setBounds(77, 167, 86, 26);
		contentPane.add(textFieldAnnee2);
		textFieldAnnee2.setColumns(10);
		
		btnValiderAnnuel = new JButton("Valider");
		btnValiderAnnuel.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnValiderAnnuel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					List<Client> liste = reportingDAO.reportingClientAnnuel(textFieldAnnee2.getText());
					textAreaReporting.setText("");
					// on affiche dans la console du client les client recus
					for (Client a : liste) {
						textAreaReporting.append(a.toString());
						textAreaReporting.append("\n");
					}
					if (liste.isEmpty()){
						JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Client");
					}
				} catch (Exception erreur) {
						JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
								"Erreur", JOptionPane.ERROR_MESSAGE);
				  }
			}
		});
		btnValiderAnnuel.setBounds(320, 169, 89, 23);
		contentPane.add(btnValiderAnnuel);
		
		
		scrollPane.setBounds(33, 201, 282, 71);
		contentPane.add(scrollPane);
		textAreaReporting.setBackground(Color.WHITE);
		
		
		scrollPane.setViewportView(textAreaReporting);
		textAreaReporting.setEditable(false);
		
		btnQuitter = new JButton("Quitter");
		btnQuitter.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnQuitter.setBounds(320, 249, 89, 23);
		contentPane.add(btnQuitter);
		
		
		
		
		
	}

}
