import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class ReportingParOperateur extends JFrame {

	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ClientFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * containeur de la fenetre
	 */
	private JPanel contentPane;
	
	/**
	 * label pour le reporting quotidien
	 */
	private JLabel lblReportingQuotidien;
	
	/**
	 * label pour le reporting mensuel
	 */
	private JLabel lblReportingMensuel;
	
	/**
	 * label pour le mois
	 */
	private JLabel lblMois;
	
	/**
	 * label annee dans le cadre du reporting mensuel
	 */
	private JLabel lblAnnee1;
	
	/**
	 * label date
	 */
	private JLabel lblDate;
	
	/**
	 * label pour le reporting annuel
	 */
	private JLabel lblReportingAnnuel;
	
	/**
	 * label pour l'annee dans le cadre du reporting annuel
	 */
	private JLabel lblAnnee2;
	
	/**
	 * zone de texte pour la date
	 */
	private JTextField textFieldDate;
	
	/**
	 * zone de texte pour le mois
	 */
	private JTextField textFieldMois;
	
	/**
	 * zone de texte pour l'annee dans le cadre du reporting mensuel
	 */
	private JTextField textFieldAnnee1;
	
	/**
	 * zone de texte pour l'annee dans le cadre du reporting annuel
	 */
	private JTextField textFieldAnnee2;
	
	/**
	 * bouton pour afficher le reporting quotidien
	 */
	private JButton btnValiderQuotidien;
	
	/**
	 * bouton pour afficher le reporting mensuel
	 */
	private JButton btnValiderMensuel;
	
	/**
	 * bouton pour afficher le reporting annuel
	 */
	private JButton btnValiderAnnuel;
	
	/**
	 * bouton pour quitter
	 */
	private JButton btnQuitter;
	
	/**
	 * instance de la classe ReportingDAO
	 */
	private ReportingDAO reportingDAO = new ReportingDAO();
	
	/**
	 * zone de teste pour afficher les reportings
	 */
	private JTextArea textAreaReporting = new JTextArea();
	
	/**
	 * zone de defilement pour les reportings
	 */
	private JScrollPane scrollPane = new JScrollPane();

	/**
	 * Ouverture de la fenetre
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReportingParOperateur frame = new ReportingParOperateur();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creation de la fenetre
	 */
	public ReportingParOperateur() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 473, 318);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Reporting par operateur");
		
		lblReportingQuotidien = new JLabel("Reporting quotidien (AAAA-MM-JJ)");
		lblReportingQuotidien.setBounds(33, 28, 214, 14);
		contentPane.add(lblReportingQuotidien);
		
		textFieldDate = new JTextField();
		textFieldDate.setBounds(77, 53, 86, 26);
		contentPane.add(textFieldDate);
		textFieldDate.setColumns(10);
		
		btnValiderQuotidien = new JButton("Valider");
		btnValiderQuotidien.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
					
						List<Operateur> liste = reportingDAO.reportingOperateurQuotidien(Date.valueOf(textFieldDate.getText()));
						textAreaReporting.setText("");
						// on affiche dans la console du client les client recus
						for (Operateur a : liste) {
							textAreaReporting.append(a.toString());
							textAreaReporting.append("\n");
						}
						if (liste.isEmpty()){
							JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Operateur");
						}
					} catch (Exception erreur) {
							JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
									"Erreur", JOptionPane.ERROR_MESSAGE);
					  }
			}
		});
		btnValiderQuotidien.setBounds(330, 55, 89, 23);
		contentPane.add(btnValiderQuotidien);
		
		lblReportingMensuel = new JLabel("Reporting Mensuel");
		lblReportingMensuel.setBounds(33, 84, 119, 14);
		contentPane.add(lblReportingMensuel);
		
		lblMois = new JLabel("Mois");
		lblMois.setBounds(33, 115, 46, 14);
		contentPane.add(lblMois);
		
		textFieldMois = new JTextField();
		textFieldMois.setBounds(77, 109, 86, 26);
		contentPane.add(textFieldMois);
		textFieldMois.setColumns(10);
		
		lblAnnee1 = new JLabel("Annee");
		lblAnnee1.setBounds(173, 115, 46, 14);
		contentPane.add(lblAnnee1);
		
		textFieldAnnee1 = new JTextField();
		textFieldAnnee1.setBounds(218, 112, 86, 26);
		contentPane.add(textFieldAnnee1);
		textFieldAnnee1.setColumns(10);
		
		btnValiderMensuel = new JButton("Valider");
		btnValiderMensuel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					List<Operateur> liste = reportingDAO.reportingOperateurMensuel(textFieldMois.getText(), textFieldAnnee2.getText());
					textAreaReporting.setText("");
					// on affiche dans la console du client les client recus
					for (Operateur a : liste) {
						textAreaReporting.append(a.toString());
						textAreaReporting.append("\n");
					}
					if (liste.isEmpty()){
						JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Operateur");
					}
				} catch (Exception erreur) {
						JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
								"Erreur", JOptionPane.ERROR_MESSAGE);
				  }
			}
		});
		btnValiderMensuel.setBounds(330, 111, 89, 23);
		contentPane.add(btnValiderMensuel);
		
		lblDate = new JLabel("Date");
		lblDate.setBounds(33, 59, 46, 14);
		contentPane.add(lblDate);
		
		lblReportingAnnuel = new JLabel("Reporting Annuel");
		lblReportingAnnuel.setBounds(33, 140, 89, 14);
		contentPane.add(lblReportingAnnuel);
		
		lblAnnee2 = new JLabel("Annee");
		lblAnnee2.setBounds(33, 171, 46, 14);
		contentPane.add(lblAnnee2);
		
		textFieldAnnee2 = new JTextField();
		textFieldAnnee2.setBounds(77, 165, 86, 26);
		contentPane.add(textFieldAnnee2);
		textFieldAnnee2.setColumns(10);
		
		btnValiderAnnuel = new JButton("Valider");
		btnValiderAnnuel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					List<Operateur> liste = reportingDAO.reportingOperateurAnnuel(textFieldAnnee2.getText());
					textAreaReporting.setText("");
					// on affiche dans la console du client les client recus
					for (Operateur a : liste) {
						textAreaReporting.append(a.toString());
						textAreaReporting.append("\n");
					}
					if (liste.isEmpty()){
						JOptionPane.showMessageDialog(new ReportingParClient(), "Aucun Operateur");
					}
				} catch (Exception erreur) {
						JOptionPane.showMessageDialog(contentPane, "Controlez vos saisies",
								"Erreur", JOptionPane.ERROR_MESSAGE);
				  }
			}
		});
		btnValiderAnnuel.setBounds(330, 167, 89, 23);
		contentPane.add(btnValiderAnnuel);
		
		
		scrollPane.setBounds(33, 197, 292, 71);
		contentPane.add(scrollPane);
		
		
		scrollPane.setViewportView(textAreaReporting);
		textAreaReporting.setEditable(false);
		
		btnQuitter = new JButton("Quitter");
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnQuitter.setBounds(330, 245, 89, 23);
		contentPane.add(btnQuitter);
	}
}
